﻿using Assignment2_SQLClient.Models;
using Assignment2_SQLClient.Repositories;
using System;
using System.Collections.Generic;

namespace Assignment2_SQLClient
{

    /// <summary>
    /// The Program Class contains all the testmethods for testing the SQL-methods in CustomerRepsitory
    /// These methods also contain printmethods to check if the queries are outputting the right data.
    /// </summary>
  public class Program
    {

        // Main Method that calls all the Testmethods, all methods should be able to run in order, but are commented out to make the console more readable.
      public  static void Main(string[] args)
        {
            CustomerRepository repository = new CustomerRepository();
            TestSelect(repository);
            //TestSelectAll(repository);
            //TestSelectCustomersLimitOffset(repository);
            //TestSelectSpecific(repository);
            
            //repository.AddNewCustomer(new Customer(-1, "Lars", "Larsen", "Norvegia", "1337", "420691337", "Lars_Ruler@Mail.no"));
            //repository.UpdateCustomer(new Customer(60, "Knut", "Knutsen", "Norvegia", "1337", "420691337", "Knut_Ruler@Mail.no"));
            
            //TestSelectCustomersPerCountry(repository);
            //TestHighestSpenders(repository);
            //TestMostPopularGenre(repository);
        }

        //Test method for GetAllCustomers.
        public static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }
       
        //Test method for GetCustomer with CustomerID as parameter..
        public static void TestSelect(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomer("3"));
        }
       
        //Test method for GetSpecificCustomer with FirstName as parameter
        public static void TestSelectSpecific(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetSpecificCustomer("Isabelle"));
        }
       
        //Test method for GetCustomerLimitOffset which takes limit and offset as parameter
        public static void TestSelectCustomersLimitOffset(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetCustomerLimitOffset(15, 2));
        }
        
        //Method that calls the PrintCustomer function for each customer in the list customers.
        public static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
       
        //Method that prints a single customer with the appropriate values.
       public static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"\t{customer.CustomerId}\t{customer.FirstName}\t{customer.LastName}\t{customer.Country}\t{customer.PostalCode}\t{customer.Phone}\t{customer.Email}\t");
        }






        /// <summary>
        /// These methods follows the same structure as Test Methods above but are made for the CustomersPerCountry method in CustomerRepository.
        /// </summary>
        /// <param name="repository"></param>

        public static void TestSelectCustomersPerCountry(ICustomerRepository repository)
        {
            PrintCustomersPerCountry(repository.CustomersPerCountry());
        }

        public static void PrintCustomersPerCountry(IEnumerable<CustomerCountry> customers)
        {
            foreach (CustomerCountry customerCountry in customers)
            {
                PrintCustomerPerCountry(customerCountry);
            }
        }

        public static void PrintCustomerPerCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"\t{customerCountry.Country}\t{customerCountry.Customers}");
        }





        /// <summary>
        /// These methods follows the same structure as the test methods but are made for the HighestSpender method in CustomerRepository.
        /// </summary>
        /// <param name="repository"></param>

        public static void TestHighestSpenders(ICustomerRepository repository)
        {
            PrintHighestSpenders(repository.HighestSpenders());
        }

        public static void PrintHighestSpenders(IEnumerable<HighestSpender> spender)
        {
            foreach (HighestSpender highestspenders in spender)
            {
                PrtintHighestSpender(highestspenders);
            }
        }

        public static void PrtintHighestSpender(HighestSpender highestspenders)
        {
            Console.WriteLine($"\t{highestspenders.ID}\t{highestspenders.Total}");
        }




        /// <summary>
        /// These methods follows the same structure as the test methods but are made for the MostPopularGenre method in CustomerRepository.
        /// </summary>
        /// <param name="repository"></param>


        public static void TestMostPopularGenre(ICustomerRepository repository)
        {
            PrintMostPopularGenres(repository.MostPopularGenre(1));
        }

        public static void PrintMostPopularGenres(IEnumerable<CustomerGenre> genre)
        {
            foreach (CustomerGenre customerGenres in genre)
            {
                PrintMostPopularGenre(customerGenres);
            }
        }

        public static void PrintMostPopularGenre(CustomerGenre customerGenre)
        {
            Console.WriteLine($"CustomerId: {customerGenre.CustomerId}\t Most Popular Genre: {customerGenre.Genre}\t Customer Name: {customerGenre.CustomerName}\t Times Featured: {customerGenre.Count}");
        }


    }
}
