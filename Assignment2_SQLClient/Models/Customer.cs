﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_SQLClient.Models
{
    /// <summary>
    /// This class is a Model made for storing the values of a Customer.
    /// </summary>
    public class Customer
    {
       public int CustomerId { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }

        public string PostalCode { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public Customer(int id, string fname, string lname, string country, string postcode, string phone, string email)
        {
            this.CustomerId = id;
            this.FirstName = fname;
            this.LastName = lname;
            this.Country = country;
            this.PostalCode = postcode;
            this.Phone = phone;
            this.Email = email;
        }

        public Customer()
        {

        }


    }
}
