﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_SQLClient.Models
{
    /// <summary>
    /// This class is a Model made for storing the values for the amount of customers per country.
    /// </summary>
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int Customers { get; set; }

        public CustomerCountry(string country, int customers)
        {
            this.Country = country;
            this.Customers = customers;
        }

        public CustomerCountry()
        {

        }
    }
}
