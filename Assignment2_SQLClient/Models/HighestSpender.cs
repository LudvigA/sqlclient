﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_SQLClient.Models
{
    /// <summary>
    /// This class is a Model made for storing the values for the customers who are the highest spenders.
    /// </summary>
    public class HighestSpender
    {
        public decimal Total { get; set; }

        public int ID { get; set; }


        public HighestSpender(decimal total, int id)
        {
            this.ID = id;
            this.Total = total;
        }

        public HighestSpender()
        {

        }
    }
}
