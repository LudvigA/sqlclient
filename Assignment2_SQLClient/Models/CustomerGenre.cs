﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_SQLClient.Models
{
    /// <summary>
    /// This class is a Model made for storing the values for the most popular genre for a customer.
    /// </summary>
    public class CustomerGenre
    {
        public string Genre { get; set; }

        public int Count { get; set; }

        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public CustomerGenre(string genre, int count, int customerid, string customername)
        {
            this.Genre = genre;
            this.Count = count;
            this.CustomerId = customerid;
            this.CustomerName = customername;
        }

        public CustomerGenre()
        {

        }
    }
}
