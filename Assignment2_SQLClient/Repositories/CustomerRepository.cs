﻿using Assignment2_SQLClient.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Assignment2_SQLClient.Repositories
{
   public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// The GetCustomer method is used to Select a customer in the database by their id.
        /// First it creates an instance of a Customer object.
        /// Then the SQL-Query is specified.
        /// The method then tries to run the query by:
        /// - Connecting to the database using GetConnectionstring and opens the connection.
        /// - Adding the parameter "CustomerId" with SQLCommand
        /// - Then the Reader tries to find this data in the Database.
        /// - If successfull it returns the customer
        /// - Else - and exception is thrown.
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Customer GetCustomer(string id)
        {
            Customer customer = new Customer();
           
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    Console.WriteLine("Done");
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            
                            Console.WriteLine($"\t{reader.GetName(0)}\t{reader.GetName(1)}\t{reader.GetName(2)}\t{reader.GetName(3)}\t{reader.GetName(4)}\t{reader.GetName(5)}\t{reader.GetName(6)}");
                            

                            while (reader.Read())
                            {         
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                           

                            }     
                        }
                    }
                    conn.Close();
                } 
            }
            catch (SqlException)
            {
                Console.WriteLine("WE FAILED");
            }
            return customer;
        }


        /// <summary>
        /// The GetAllCustomers method is used to Select all customers in the database .
        /// First it creates a list which is used to store the customers.
        /// Then the SQL-Query is specified.
        /// The method then tries to run the query by:
        /// - Connecting to the database using GetConnectionstring and opens the connection. 
        /// - Then the Reader tries to find every customer in the Database and Add it to the list.
        /// - If successfull it returns the customerlist
        /// - Else - and exception is thrown.
        ///
        /// </summary>
        /// <returns></returns>

        public List<Customer> GetAllCustomers()
        {
       
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT  CustomerId, FirstName, LastName, ISNULL(Country,''), ISNULL(PostalCode,''), ISNULL(Phone,''), ISNULL(Email,'') FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                           
                            while (reader.Read())
                            {

                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4);
                                temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);


                                custList.Add(temp);
                            }
                        }
                    }
                    conn.Close();
                }  
            }
            catch (SqlException)
            {
                Console.WriteLine("You Failed");
            }
            return custList;

        }



        /// <summary>
        /// The GetSpecificCustomer method is used to Select a specific customer by FirstName from the database .
        /// First it creates an instance of the Customer Object.
        /// Then the SQL-Query is specified.
        /// The method then tries to run the query by:
        /// - Connecting to the database using GetConnectionstring and opens the connection. 
        /// - Adding the parameter "Firstname as name" with SQLCommand.
        /// - Then the Reader tries to find the specific customer.
        /// - If successfull it returns the customer.
        /// - Else - and exception is thrown.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Customer GetSpecificCustomer(string name)
        {
            Customer customer = new Customer();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE FirstName LIKE @FirstName";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    Console.WriteLine("Done");
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", name);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            //Console.WriteLine(reader.GetName(2));
                            Console.WriteLine($"\t{reader.GetName(0)}\t{reader.GetName(1)}\t{reader.GetName(2)}\t{reader.GetName(3)}\t{reader.GetName(4)}\t{reader.GetName(5)}\t{reader.GetName(6)}");


                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);


                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException)
            {
                Console.WriteLine("WE FAILED");
            }
            return customer;
        }



        /// <summary>
        /// The GetCustomerLimitOffset method is used to return a page of customer based on specified limit and offset .
        /// First it creates a list which is contain the page of customers
        /// Then the SQL-Query is specified.
        /// The method then tries to run the query by:
        /// - Connecting to the database using GetConnectionstring and opens the connection. 
        /// - Adding the parameter "limit" and "offset" with SQLCommand.
        /// - Then the Reader tries to the customers based on the limit and offset and adds it to the list.
        /// - If successfull it returns the list of customers.
        /// - Else - and exception is thrown
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public List<Customer> GetCustomerLimitOffset(int limit, int offset)
        {

            List<Customer> offsetList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Chinook.dbo.Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@limit", limit);
                        cmd.Parameters.AddWithValue("@offset", offset);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            Console.WriteLine($"\t{reader.GetName(0)}\t{reader.GetName(1)}\t{reader.GetName(2)}\t{reader.GetName(3)}\t{reader.GetName(4)}\t{reader.GetName(5)}\t{reader.GetName(6)}\t");
                            while (reader.Read())
                            {

                                Customer temp = new Customer();


                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4);
                                temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);


                                offsetList.Add(temp);
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException)
            {
                Console.WriteLine("You Failed");
            }
            return offsetList;

        }



        /// <summary>
        /// The AddNewCustomerFunction is used to add a new instance of a customer to the database.
        /// The values of the row is specified in paramters for the Test Method in the Program Class.
        /// Based on these parameters and the SQL-query, the method adds these values to the customer.
        /// if this fails and exception will be thrown.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool AddNewCustomer(Customer customer)
        {

            
            bool success = false;
            string sql = "INSERT INTO Chinook.dbo.Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException)
            {
                Console.WriteLine("You Failed...miserably");
            }
            return success;
        }

        /// <summary>
        /// The UpdateCustomer method is used to update an existing customer in the Chinook database.
        /// This is done by referencing to the CustomerId of the customer you want to update.
        /// If the CustomerId specified exists in the database, the rows of that customer will be 
        /// updated based on the parameters given in the method.
        /// Else - An exception will be thrown.
        /// 
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Chinook.dbo.Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email" +
                " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException)
            {
                Console.WriteLine("You Failed Even Harder");
            }
            return success;
        }

        /// <summary>
        /// The CustomersPerCountry method is used to count the number of customers in each country.
        /// First it creates a list which is used to store the countries and amount of customers.
        /// Then the SQL-Query is specified.
        /// The method then tries to run the query by:
        /// - Connecting to the database using GetConnectionstring and opens the connection. 
        /// - Then the Reader tries to find every country and amount of customers in the Database and Add it to the list.
        /// - If successfull it returns the list.
        /// - Else - and exception is thrown.
        ///
        /// <returns></returns>
        public List<CustomerCountry> CustomersPerCountry()
        {
            List<CustomerCountry> customerCountries = new();

            string sql = "SELECT Country, COUNT(*) AS number FROM Customer GROUP BY Country ORDER BY number DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customerCountry = new();
                                customerCountry.Country = reader.GetString(0);
                                customerCountry.Customers = reader.GetInt32(1);
                                customerCountries.Add(customerCountry);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerCountries;
        }


        /// <summary>
        /// The HighestSpenders method is used to find the highest spenders.
        /// First it creates a list which is used to store the highest spenders.
        /// Then the SQL-Query is specified.
        /// The method then tries to run the query by:
        /// - Connecting to the database using GetConnectionstring and opens the connection. 
        /// - Then the Reader tries to find customers and their amount spent.
        /// - If successfull it returns the list.
        /// - Else - and exception is thrown.
        ///
        /// <returns></returns>

        public List<HighestSpender> HighestSpenders()
        {
            List<HighestSpender> highestSpenders = new();

            string sql = "SELECT CustomerId,  SUM(Total) AS sum FROM Invoice GROUP BY CustomerId ORDER BY sum DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                HighestSpender highestspenders = new();
                                highestspenders.ID = reader.GetInt32(0);
                                highestspenders.Total = reader.GetDecimal(1);
                                highestSpenders.Add(highestspenders);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return highestSpenders;
        }

        /// <summary>
        /// The MostPopularGenre method is used to find the most popular genre for a given customer based on most tracks from invoices.
        /// if tied - both are displayed
        ///  First it creates a list which is used to store the customer(s).
        /// Then the SQL-Query is specified.
        /// The method then tries to run the query by:
        /// - Connecting to the database using GetConnectionstring and opens the connection. 
        /// - Then the Reader tries to find customer and the most popular genre.
        /// - If successfull it returns the list.
        /// - Else - and exception is thrown.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<CustomerGenre> MostPopularGenre(int customerId)
        {
            List<CustomerGenre> customerGenres = new();

            string sql =
                "SELECT TOP 1 WITH TIES Customer.CustomerId, Customer.FirstName, Genre.Name, COUNT(Genre.GenreId) AS number " +
                "FROM ((((Customer " +
                "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId) " +
                "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId) " +
                "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId) " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreId) " +
                "WHERE Customer.CustomerId = @CustomerId " +
                "GROUP BY Customer.CustomerId, Customer.FirstName, Genre.Name " +
                "ORDER BY number DESC ";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerId", customerId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                CustomerGenre customerGenre = new();
                                customerGenre.CustomerId = reader.GetInt32(0);
                                customerGenre.CustomerName = reader.GetString(1);
                                customerGenre.Genre = reader.GetString(2);
                                customerGenre.Count = reader.GetInt32(3);

                                customerGenres.Add(customerGenre);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerGenres;
        }







    }


}



