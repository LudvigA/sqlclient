﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2_SQLClient.Models;
using Microsoft.Data.SqlClient;

namespace Assignment2_SQLClient.Repositories
{

    /// <summary>
    /// Interface that specifies all the methods that are used in the repository.
    /// </summary>
    public interface ICustomerRepository
    {
        public Customer GetCustomer(string id);
        public List<Customer> GetAllCustomers();
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public List<Customer> GetCustomerLimitOffset(int limit, int offset);

        public List<CustomerCountry> CustomersPerCountry();

        public List<HighestSpender> HighestSpenders();

        public Customer GetSpecificCustomer(string name);

        public List<CustomerGenre> MostPopularGenre(int customerId);

    }
}
