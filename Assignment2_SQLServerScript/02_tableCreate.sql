CREATE TABLE SuperheroesDb.dbo.Superhero(
	ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Name NvarChar(50),
	Alias NvarChar(50),
	Origin NvarChar(50)
	);

CREATE TABLE SuperheroesDb.dbo.Assistant(
	ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Name NvarChar(50)
	);

CREATE TABLE SuperheroesDb.dbo.Power(
	ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Name NvarChar(50),
	Description NvarChar(50)
	);