CREATE TABLE SuperheroesDb.dbo.Superhero_Powers(
	SuperheroID int NOT NULL,
	PowerID int NOT NULL,
	CONSTRAINT SuperheroPowersID PRIMARY KEY (SuperheroID, PowerID),
	CONSTRAINT FK_Superhero
		FOREIGN KEY (SuperheroID) REFERENCES Superhero (ID),
	CONSTRAINT FK_Power
		FOREIGN KEY (PowerID) REFERENCES Power (ID)
	);
