ALTER TABLE SuperheroesDb.dbo.Assistant
      ADD Superhero_ID int not null

ALTER TABLE SuperheroesDb.dbo.Assistant
ADD CONSTRAINT Superhero_ID FOREIGN KEY (Superhero_ID)
      REFERENCES SuperHeroesDb.dbo.SuperHero (ID)

