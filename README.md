# SQLClient

## Name
Data Access With SQL Client

## Description
Includes SQL Server Scripts used to create and modify database in SQL Server Management Studio, as well as a SQL Client for accessing data in a database. The SQL Client is composed using repository patterns and implements methods to handle data from an existing database.

## Installation

Install Microsoft.Data.SqlClient

``````bash
dotnet add [PROJECT NAME] package Microsoft.Data.SqlClient --version 3.0.0
``````

## Usage
For information about usage, look at the implemented summary tags.

For the SQL Client project:
	In program.cs, uncomment the desired lines (line 21-30) to run the various methods.

For the SQLServerScript:
	The various scripts are supplied in such a way that they can be run in the supplemented order.